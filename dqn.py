from typing import OrderedDict
import torch as T
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np


class DQN(nn.Module):
    def __init__(self, learning_rate: float, state_dims: int, fc1_dims: int, fc2_dims: int, n_actions: int) -> None:
        super().__init__()

        # Network hyperparameters
        self.input_dims = state_dims
        self.fc1_dims = fc1_dims
        self.fc2_dims = fc2_dims
        self.output_dims = n_actions

        self.learning_rate = learning_rate

        # Layers
        self.model = nn.Sequential(OrderedDict({
            # the unpacking makes the code more extensible
            "fully_connected_1": nn.Linear(self.input_dims, self.fc1_dims),
            "relu_1": nn.ReLU(),
            "fully_connected_2": nn.Linear(self.fc1_dims, self.fc2_dims),
            "relu_2": nn.ReLU(),
            "fully_connected_3": nn.Linear(self.fc2_dims, self.output_dims)
        }))

        # Optimizer
        self.optimizer = optim.Adam(self.parameters(), lr=self.learning_rate)

        # Loss
        self.loss_fn = nn.MSELoss()

        # Replay memory (target network not needed)
        self.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')
        self.to(self.device)

    def optimizer_zero_grad(self) -> None:
        self.optimizer.zero_grad()

    def forward(self, x: T.Tensor) -> T.Tensor:
        return self.model.forward(x)

    def compute_loss(self, target: T.Tensor, actual: T.Tensor) -> None:
        loss_val = self.loss_fn.forward(actual, target).to(self.device)
        loss_val.backward()
        self.optimizer.step()

    def to_device_tensor(self, np_array: np.ndarray) -> T.Tensor:
        return T.tensor(np_array).to(self.device)
