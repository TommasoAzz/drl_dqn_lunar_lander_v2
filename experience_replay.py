from typing import Tuple
import torch as T
import numpy as np


class ExperienceReplay:
    def __init__(self, mem_size: int, state_dims: int) -> None:
        # Fixed state
        self.memory_size = mem_size
        self.state_dims = state_dims

        # Mutable state
        self.memory_counter = 0
        self.state_memory = np.zeros((self.memory_size, self.state_dims), dtype=np.float32)
        self.new_state_memory = np.zeros((self.memory_size, self.state_dims), dtype=np.float32)
        self.action_memory = np.zeros(self.memory_size, dtype=np.int32)
        self.reward_memory = np.zeros(self.memory_size, dtype=np.float32)
        self.terminal_memory = np.zeros(self.memory_size, dtype=bool)

    def store(self, state: np.ndarray, action: np.int32, reward: np.float32, new_state: np.ndarray, done: bool) -> None:
        index = self.memory_counter % self.memory_size
        self.state_memory[index] = state
        self.new_state_memory[index] = new_state
        self.action_memory[index] = action
        self.reward_memory[index] = reward
        self.terminal_memory[index] = done

        self.memory_counter += 1
    
    def batch(self, size: int) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        max_mem = min(self.memory_counter, self.memory_size)
        
        batch = np.random.choice(max_mem, size, replace=False)
        batch_index = np.arange(size, dtype=np.int32)

        return (
            batch_index,
            self.state_memory[batch],
            self.new_state_memory[batch],
            self.action_memory[batch],
            self.reward_memory[batch],
            self.terminal_memory[batch]
        )