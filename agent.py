import torch as T
import numpy as np

from dqn import DQN
from experience_replay import ExperienceReplay


class Agent:
    def __init__(self,
                 gamma: float,
                 epsilon: float,
                 learning_rate: float,
                 state_dims: int,
                 batch_size: int,
                 n_actions: int,
                 target_network_update_every: int = 10000,
                 replay_mem_size: int = 100000,
                 epsilon_end: float = 0.01,
                 epsilon_decay: float = 5e-4) -> None:
        # Network
        self.Q_network  = DQN(
            state_dims=state_dims,
            fc1_dims=256,
            fc2_dims=256,
            n_actions=n_actions,
            learning_rate=learning_rate
        )
        
        # Network hyperparameters
        self.gamma = gamma
        self.epsilon = epsilon
        self.batch_size = batch_size
        self.action_space = [i for i in range(n_actions)]
        self.epsilon_end = epsilon_end
        self.epsilon_decay = epsilon_decay

        # Experience replay
        self.experience_replay = ExperienceReplay(replay_mem_size, state_dims)

        # Target network
        self.update_counter = 0
        self.target_network_update_every = target_network_update_every
        self.target_network = DQN(
            state_dims=state_dims,
            fc1_dims=256,
            fc2_dims=256,
            n_actions=n_actions,
            learning_rate=learning_rate
        )

    def store_transition(self, state: np.ndarray, action: np.int32, reward: np.float32, new_state: np.ndarray, done: bool) -> None:
        self.experience_replay.store(state, action, reward, new_state, done)

    def choose_action(self, observation: np.ndarray) -> np.int32:
        # epsilon-greedy policy
        if np.random.random() > self.epsilon:
            state = self.Q_network.to_device_tensor(observation)
            actions = self.Q_network.forward(state)
            action = T.argmax(actions).item()
        else:
            action = np.random.choice(self.action_space)
        
        return action

    def learn(self) -> None:
        if self.experience_replay.memory_counter < self.batch_size:
            print("Cannot learn: The number of transitions stored in the experience replay is still below the batch size.")
            return

        self.Q_network.optimizer_zero_grad()
        self.target_network.optimizer_zero_grad()

        batch_index, state, new_state, action, reward, terminal = self.experience_replay.batch(self.batch_size)
        t_state = self.Q_network.to_device_tensor(state)
        t_new_state = self.Q_network.to_device_tensor(new_state)
        t_reward = self.Q_network.to_device_tensor(reward)
        t_terminal = self.Q_network.to_device_tensor(terminal)

        q_eval = self.Q_network.forward(t_state)[batch_index, action] # I want to update the biases only for the actions that were taken
        q_next = self.target_network.forward(t_new_state)  # this would be for the target network
        q_next[t_terminal] = 0.0  # the bias of the terminal states are zero

        q_target = t_reward + self.gamma * T.max(q_next, dim=1)[0]  # dim=1 because we want the actions, [0] because values and indexes are returned

        self.Q_network.compute_loss(q_target, q_eval)

        self.epsilon = (self.epsilon - self.epsilon_decay) if self.epsilon > self.epsilon_end else self.epsilon_end

        self.update_counter += 1
        if self.update_counter == self.target_network_update_every:
            self.target_network.load_state_dict(self.Q_network.state_dict())
            self.update_counter = 0
