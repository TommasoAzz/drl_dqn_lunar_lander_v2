import gym
import numpy as np

from agent import Agent

if __name__ == '__main__':
    env = gym.make("LunarLander-v2", new_step_api=True)
    agent = Agent(
        gamma=0.99,
        epsilon=0.9,
        batch_size=64,
        n_actions=env.action_space.n,
        epsilon_end=0.01,
        state_dims=env.observation_space.shape[0],
        learning_rate=0.003
    )

    scores = []
    epsilon_history = []
    episodes = 500

    for i in range(episodes):
        score = 0
        done = False
        state = env.reset()
        while not done:
            action = agent.choose_action(state)
            new_state, reward, done, truncated, _ = env.step(action)
            score += reward
            agent.store_transition(state, action, reward, new_state, done)
            agent.learn()
            state = new_state
        scores.append(score)
        epsilon_history.append(agent.epsilon)

        avg_score = np.mean(scores[-100:])
        print("Episode", i, "\tscore %.2f" % score, "\tavg score %.2f" % avg_score, "\tepsilon %.2f" % agent.epsilon)